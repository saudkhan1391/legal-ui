# base image
FROM node:12.12.0

# install and cache app dependencies
RUN npm install yarn serve -g --silent

# set working directory
RUN mkdir /app
WORKDIR /app

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH
ENV NODE_ENV production

COPY package.json yarn.lock /app/
RUN yarn

COPY . /app/
RUN npm run build
RUN chmod +x env.sh

EXPOSE 5000

# start app
CMD ["node", "server.js"]