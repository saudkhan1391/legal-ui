import { UPDATE_OAUTH_DATA } from './const';

export const updateOAuthData = (data) => ({
  type: UPDATE_OAUTH_DATA,
  data
});
