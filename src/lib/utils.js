import { fetchGET } from '@empire/shared-ui/fetch';
import { toast } from 'react-toastify';

import { appSettings } from 'settings';

export const CONSTANTS = {
  GET_STARTED: 'on-boarding',
  DASHBOARD: 'dashboard'
};

// Generates initial route to be used after successfull login
export const generatePostAuthRoute = (user = {}) => {
  const { userType, profileCompleted } = user;
  const $routePrefix = userType === 20 ? '/advocate' : '';
  const route = profileCompleted ? CONSTANTS.DASHBOARD : CONSTANTS.GET_STARTED;
  return `${$routePrefix}/${route}`;
};

export const scrollToRef = (ref, e) => {
  if (!ref || !ref.current) {
    return;
  }
  if (e) {
    e.preventDefault();
  }
  ref.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
};

export const generateOauthURL = (provider = 'LinkedIn', user) => {
  const userType = user ? 20 : 10;
  return `${appSettings.apiBaseUrl}/account/external_login/${provider}?redirecturl=${window.__ENV__.APP_URL}/oauth_callback/${userType}`;
};

export const getUserData = (
  stateContext,
  onSuccess,
  onError = (error) => {
    toast.error(error);
  },
  onCompleted
) => {
  fetchGET(`${appSettings.apiBaseUrl}/profile/get_user_data`, {
    stateContext,
    onSuccess,
    onError: (error) => {
      toast.error(error);
      onError();
    },
    onCompleted
  });
};
