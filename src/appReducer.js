import locale from 'browser-locale';
import Cookies from 'js-cookie';

import { Record } from 'immutable';

import I18n from '@empire/shared-ui/i18n';

import {
  UPDATE_USER,
  LOGOUT_USER,
  UPDATE_LANGUAGE
} from '@empire/shared-ui/actions/const';
import { UPDATE_OAUTH_DATA } from './lib/const';

const isNL = /nl/i.test(locale());
const val = Cookies.get('.LegalLanguage');

const i18n = new I18n();
i18n.addLanguage('EN', window.LocalizedStringsEN);
i18n.addLanguage('NL', window.LocalizedStringsNL);
i18n.setLanguage(val || (isNL ? 'NL' : 'EN'));

const User = new Record({
  key: null,
  firstName: null,
  middleName: null,
  lastName: null,
  email: null,
  slug: null,
  avatarUrl: null,
  profileCompleted: null,
  phone: null,
  caseKey: null,
  userType: null
});

const OAuthData = new Record({
  firstName: null,
  lastName: null,
  email: null
});

const State = new Record({
  user: null,
  isAuthenticated: false,
  oauthData: OAuthData,
  i18n
});

const initialState = new State();

function appReducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_USER: {
      const user = new User(action.user);

      return state.set('isAuthenticated', true).set('user', user);
    }

    case LOGOUT_USER: {
      return state.set('isAuthenticated', false).set('user', null);
    }

    case UPDATE_OAUTH_DATA: {
      return state.set('oauthData', new OAuthData(action.data));
    }

    case UPDATE_LANGUAGE: {
      Cookies.set('.LegalLanguage', action.language, { expires: 365 });

      window.location.reload();

      return state;
    }

    default:
      return state;
  }
}

export default appReducer;
