import React, { Component } from 'react';

import IconButton from 'components/core/IconButton';

class ViewSummons extends Component {
  static propTypes = {};

  render() {
    return (
      <div>
        <div className="view-box">
          <h5>Dagvaarding</h5>

          <div className="text">
            <label className="title">Sector</label>
            <label>: 10</label>
          </div>

          <div className="text">
            <label className="title">Parketnr</label>
            <label>: 15/010017-04</label>
          </div>

          <div className="text">
            <label className="title">volgnr</label>
            <label>: 0001</label>
          </div>

          <div className="text">
            <label className="title">volgnr</label>
            <label>: 0001</label>
          </div>

          <div className="text">
            <label className="title mt-5">Ana</label>
            <div className="sub-datail">
              <div className="text">
                <label className="title">naam</label>
                <label>: Verschuur</label>
              </div>

              <div className="text">
                <label className="title">voornamen</label>
                <label>: Jolien</label>
              </div>

              <div className="text">
                <label className="title">geboren op</label>
                <label>: 3 mei 1981 te Amsterdam</label>
              </div>

              <div className="text">
                <label className="title">wonede te</label>
                <label>: Amsterdam</label>
              </div>

              <div className="text">
                <label className="title">verblijvende</label>
                <label>: huis van bewaring Weteringschans</label>
              </div>

              <div className="text">
                <label className="title">adres</label>
                <label>: Kraaiennest 22</label>
              </div>
            </div>
          </div>

          <div className="text mt-5">
            <label>
              Hierbij dagvaard ik u om als verdachte te cerschinen op 9 mei 2017
              om15:30, ter terechtzitting van de meervoudige strafkamer in de
              arrondissementsrecht te Amsterdam, Parnassusweg 220, teneinde
              terecht te staan terzake van hetgeen hieronder is omschreven.
            </label>
          </div>
        </div>

        <IconButton label="Afsluiten" />
      </div>
    );
  }
}

ViewSummons.propTypes = {};

export default ViewSummons;
