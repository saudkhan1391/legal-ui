import React, { Component } from 'react';

import OnBoardingFormContainer from 'components/common/onBoarding/onBoardingFormContainer';
import ProgressBar from 'components/core/ProgressBar';

const images = {
  binocularGuy: require('components/../assets/images/binocular-guy.svg')
};

class StepFourForm extends Component {
  render() {
    return (
      <OnBoardingFormContainer>
        <div className="processing-form-container">
          <h4>Houd uw e-mail in de gaten!</h4>
          <p>
            U kunt nu gerust uitloggen, u krijgt een bericht wanneer er nieuwe
            dagvaardingen/verzoekschriften geplaatst zijn.
          </p>
          <img src={images.binocularGuy} alt="binocular-guy" />

          <div className="progress-indicator">
            <ProgressBar progress={40} color="black" height={22} />
            <p>Processing 40%</p>
          </div>
        </div>
      </OnBoardingFormContainer>
    );
  }
}

export default StepFourForm;
