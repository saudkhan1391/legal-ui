import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import { fetchGET, fetchPOST } from '@empire/shared-ui/fetch';

import Input from '@empire/hydra/components/Input';
import Button from '@empire/hydra/components/Button';
import Loader from '@empire/hydra/components/Loader';

import SplitInputRow from 'components/common/onBoarding/SplitInputRow';
import OnBoardingFormContainer from 'components/common/onBoarding/onBoardingFormContainer';

import { appSettings } from 'settings';

class StepTwoForm extends Component {
  static propTypes = {
    onSuccess: PropTypes.func,
  };

  static defaultProps = {
    onSuccess: () => {}
  };

  state = {
    officeName: '',
    postCode: '',
    houseNumber: '',
    street: 'Auto fetched',
    place: 'Auto fetched',
  };

  componentDidMount() {
    fetchGET(`${appSettings.apiBaseUrl}/advocate/on_boarding_data`, {
      stateContext: this,
      onSuccess: (json) => {
        this.setState({ loader: false }, () => {
          if (json.success) {
            const { houseNo, name, place, postcode, street, } = json.data.officeRequest;
            this.setState({
              officeName: name,
              postCode: postcode,
              houseNumber: houseNo,
              street,
              place
            });
          } else {
            toast.error('error');
          }
        });
      },
      onError: (error) => {
        this.setState({ loader: false }, () => {
          toast.error(`${error}`);
        });
      }
    });
  }

  sendOfficeDetail = () => {
    const { onSuccess } = this.props;
    const { officeName, postCode, houseNumber, street, place } = this.state;

    this.setState({ loader: true }, () => {
      fetchPOST(
        `${appSettings.apiBaseUrl}/advocate/office`,
        {
          name: officeName,
          postCode,
          houseNo: houseNumber,
          street,
          place
        },
        {
          stateContext: this,
          onSuccess: (json) => {
            this.setState({ loader: false }, () => {
              if (json.success) {
                onSuccess();
              }
            });
          },
          onError: (error) => {
            this.setState({ loader: false }, () => {
              toast.error(`errors.${error}`);
            });
          }
        }
      );
    });
  };

  render() {
    const { loader } = this.state;

    return (
      <OnBoardingFormContainer title="Kantoorgegevens">
        <SplitInputRow label="Naam kantoor">
          <Input
            value={this.state.officeName}
            onChange={(r) => this.setState({ officeName: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Postcode">
          <Input
            value={this.state.postCode}
            onChange={(r) => this.setState({ postCode: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Huisnummer">
          <Input
            value={this.state.houseNumber}
            onChange={(r) => this.setState({ houseNumber: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Straat">
          <Input
            // disabled
            value={this.state.street}
            onChange={(r) => this.setState({ street: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Plaatsnaam">
          <Input
            // disabled
            value={this.state.place}
            onChange={(r) => this.setState({ place: r })}
          />
        </SplitInputRow>

        <hr className="horizontal-rule" />

        <div className="row form-item justify-content-end">
          {loader ? (
            <Loader margin={false} />
          ) : (
            <Button
              appendIcon="fas fa-long-arrow-alt-right"
              onClick={this.sendOfficeDetail}
            >
              Volgende stap
            </Button>
          )}
        </div>
      </OnBoardingFormContainer>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(StepTwoForm);
