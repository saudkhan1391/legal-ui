import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import RSelect from 'react-select';
import makeAnimated from 'react-select/animated';

import Button from '@empire/hydra/components/Button';
import Select from '@empire/hydra/components/Select';
import RadioGroup from '@empire/hydra/components/RadioGroup';
import CheckboxGroup from '@empire/hydra/components/CheckboxGroup';
import Input from '@empire/hydra/components/Input';
import Popup from '@empire/hydra/components/Popup';
import Loader from '@empire/hydra/components/Loader';

import { fetchGET, fetchPOST } from '@empire/shared-ui/fetch';

import OnBoardingFormContainer from 'components/common/onBoarding/onBoardingFormContainer';
import SplitInputRow from 'components/common/onBoarding/SplitInputRow';

import { appSettings } from 'settings';

const options = [
  { label: 'Ja', value: true },
  { label: 'Nee', value: false }
];

class StepThreeForm extends Component {
  static propTypes = {
    onSuccess: PropTypes.func
  };

  static defaultProps = {
    onSuccess: () => {},
  };

  state = {
    popupOpen: false,
    yearSworn: 'false',
    additionalCases: 'false',
    expenseMatters: 'false',
    emergencyFacilities: 'false',
    language: 'false',
    languages: [],
    preferredLawTypes: [],
    CheckboxOptions: [],
    perks: [],
    loader: false
  };

  componentDidMount() {
    this.loadLawTypes();
  }

  loadLanguages() {
    fetchGET(`${appSettings.apiBaseUrl}/service/languages`, {
      stateContext: this,
      onSuccess: (json) => {
        this.setState({ loader: false }, () => {
          if (json.success) {
            this.setState({
              CheckboxOptions: json.data
            });
          } else {
            toast.error('error');
          }
        });
      },
      onError: (error) => {
        this.setState({ loader: false }, () => {
          toast.error(`${error}`);
        });
      }
    });
  }

  loadLawTypes() {
    fetchGET(`${appSettings.apiBaseUrl}/service/law_types`, {
      stateContext: this,
      onSuccess: (json) => {
        this.setState({ loader: false }, () => {
          // console.log("Hello", json);
          if (json.success) {
            this.setState({
              perks: json.data.map((j) => ({
                value: j.id,
                label: j.name
              })),
            }, () => {
              const { perks } = this.state;
              fetchGET(`${appSettings.apiBaseUrl}/advocate/on_boarding_data`, {
                stateContext: this,
                onSuccess: (secondJson) => {
                  this.setState({ loader: false }, () => {
                    if (secondJson.success) {
                      const { yearSworn, additionalCases, expenseMatters, emergencyFacilities, languages, preferredLawTypes } = secondJson.data.advocatePreferenceRequest;
                      this.setState({
                        additionalCases: `${additionalCases}`,
                        emergencyFacilities: `${emergencyFacilities}`,
                        expenseMatters: `${expenseMatters}`,
                        languages,
                        language: languages.length !== 0 ? 'true' : 'false',
                        preferredLawTypes: perks.filter((p) => preferredLawTypes.indexOf(p.value) !== -1),
                        yearSworn: `${yearSworn}`,
                      });
                    } else {
                      toast.error('error');
                    }
                  });
                },
                onError: (error) => {
                  this.setState({ loader: false }, () => {
                    toast.error(`${error}`);
                  });
                }
              });
            });
          } else {
            toast.error('error');
          }
        });
      },
      onError: (error) => {
        this.setState({ loader: false }, () => {
          toast.error(`${error}`);
        });
      }
    });
  }

  convertToBool = (value) => {
    const val = (value === 'true');
    return val;
  }

  sendPreferences = () => {
    const { onSuccess } = this.props;
    // onSuccess();

    const { yearSworn, additionalCases, expenseMatters, emergencyFacilities, languages } = this.state;
    let { preferredLawTypes } = this.state;
    const additionalCasesBool = this.convertToBool(additionalCases);
    const expenseMattersBool = this.convertToBool(expenseMatters);
    const emergencyFacilitiesBool = this.convertToBool(emergencyFacilities);
    preferredLawTypes = preferredLawTypes.map((p) => p.value);

    this.setState({ loader: true }, () => {
      fetchPOST(
        `${appSettings.apiBaseUrl}/advocate/preferences`,
        {
          yearSworn,
          additionalCases: additionalCasesBool,
          expenseMatters: expenseMattersBool,
          emergencyFacilities: emergencyFacilitiesBool,
          languages,
          preferredLawTypes
        },
        {
          stateContext: this,
          onSuccess: (json) => {
            this.setState({ loader: false }, () => {
              if (json.success) {
                onSuccess();
              }
            });
          },
          onError: (error) => {
            this.setState({ loader: false }, () => {
              toast.error(`errors.${error}`);
            });
          }
        }
      );
    });
  }

  perksChange = (preferredLawTypes) => {
    this.setState({ preferredLawTypes });
  }

  render() {
    const { onSuccess } = this.props;
    const { yearSworn, additionalCases, expenseMatters, emergencyFacilities, languages, language, CheckboxOptions, perks, preferredLawTypes, popupOpen, loader } = this.state;

    const currentYear = (new Date()).getFullYear();
    const years = Array.from(new Array(50), (val, index) => currentYear - index);
    const list = [];
    years.map((year, index) => {
      list.push({ label: year, value: year });
    });

    return (
      <OnBoardingFormContainer
        title="Aanvullende vragen"
        customStyle="custom-radio-box"
      >
        <SplitInputRow label="Jaar beëdiging">
          <Select
            options={list}
            value={yearSworn}
            onChange={(r) => this.setState({ yearSworn: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Treedt u op in toevoegingszaken?">
          <RadioGroup
            options={options}
            horizontal
            value={additionalCases}
            onChange={(r) => this.setState({ additionalCases: r })}
            required
          />
        </SplitInputRow>

        <SplitInputRow label="Treedt u op in rechtsbijstandverzekering zaken?">
          <RadioGroup
            options={options}
            horizontal
            value={emergencyFacilities}
            onChange={(r) => this.setState({ emergencyFacilities: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Treedt u op in spoedvoorzieningen?">
          <RadioGroup
            options={options}
            horizontal
            value={expenseMatters}
            onChange={(r) => this.setState({ expenseMatters: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Heeft u een voorkeurstaal die anders is dan Nederlands?">
          <Popup manualControl isOpen={popupOpen}>
            <Popup.Activator>
              <RadioGroup
                options={options}
                horizontal
                required
                value={language}
                onChange={(r) => {
                  this.setState({ language: r });
                  const val = (r === 'true');
                  if (val) {
                    this.setState({ popupOpen: true }, () => {
                      this.loadLanguages();
                    });
                  } else {
                    this.setState({ popupOpen: false, languages: [1] });
                  }
                }}
              />
            </Popup.Activator>
            <Popup.Content>
              <div className="row no-gutters">
                <div className="col-12">
                  <Input
                    value={this.state.searchValue}
                    onChange={(r) => console.log(r)}
                  />
                </div>
                <div className="col-12 d-flex justify-content-between mt-3">
                  <label>SELECTEER ALLE</label>
                  <a>RESET</a>
                </div>
                <div className="col-12">
                  <CheckboxGroup
                    dataText="name"
                    dataValue="id"
                    value={languages}
                    options={CheckboxOptions}
                    onChange={(r) => this.setState({ languages: r })}
                  />
                </div>
                <hr className="horizontal-rule" />

                <div className="col-12">
                  <Button
                    appendIcon="fas fa-long-arrow-alt-right"
                    onClick={() => {
                      if (languages.length !== 0) {
                        this.setState({ popupOpen: false });
                      }
                    }}
                  >
                    Toevoegen
                  </Button>
                </div>
              </div>
            </Popup.Content>
          </Popup>
        </SplitInputRow>

        <SplitInputRow label="Op welke rechtsgebieden bent u werkzaam">
          <RSelect
            placeholder="Select perks &amp; benefits"
            components={makeAnimated()}
            closeMenuOnSelect={false}
            options={perks}
            value={preferredLawTypes}
            isMulti
            onChange={this.perksChange}
          />
        </SplitInputRow>

        <hr className="horizontal-rule" />

        <div className="row form-item justify-content-end">
          {loader ? (
            <Loader margin={false} />
          ) : (
            <Button
              appendIcon="fas fa-long-arrow-alt-right"
              onClick={this.sendPreferences}
            >
              Volgende stap
            </Button>
          )}
        </div>
      </OnBoardingFormContainer>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.app.user
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(StepThreeForm);
