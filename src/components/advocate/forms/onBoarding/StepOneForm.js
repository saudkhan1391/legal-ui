import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import Cookies from 'js-cookie';

import RadioGroup from '@empire/hydra/components/RadioGroup';
import Button from '@empire/hydra/components/Button';
import { fetchPOST } from '@empire/shared-ui/fetch';

import SplitInputRow from 'components/common/onBoarding/SplitInputRow';
import OnBoardingFormContainer from 'components/common/onBoarding/onBoardingFormContainer';
import Input from '@empire/hydra/components/Input';
import Checkbox from '@empire/hydra/components/Checkbox';
import Loader from '@empire/hydra/components/Loader';

import { appSettings } from 'settings';

const genderOptions = [
  { label: 'Man', value: 'Male' },
  { label: 'Vrouw', value: 'Female' }
];

class StepOneForm extends Component {
  static propTypes = {
    onSuccess: PropTypes.func,
    user: PropTypes.object.isRequired,
  };

  static defaultProps = {
    onSuccess: () => {}
  };

  state = {
    barNumber: '',
    dateOfBirth: '',
    gender: null,
    initials: '',
    firstName: '',
    middleName: '',
    lastName: '',
    phone: '',
    email: '',
    password: '',
    confirmPassword: '',
    acceptTerms: false,
    loader: false
  };

  componentDidMount() {
    const { user, onSuccess } = this.props;
    if (user !== null) {
      onSuccess();
    }
  }

  signUp = () => {
    const { onSuccess } = this.props;

    const {
      barNumber,
      dateOfBirth,
      gender,
      initials,
      firstName,
      middleName,
      lastName,
      phone,
      email,
      password,
      confirmPassword,
      acceptTerms,
      loader
    } = this.state;

    this.setState({ loader: !loader }, () => {
      fetchPOST(
        `${appSettings.apiBaseUrl}/account/register`,
        {
          barNumber,
          dateOfBirth,
          initials,
          firstName,
          middleName,
          lastName,
          phone,
          email,
          password,
          acceptTerms,
          gender,
          UserType: 20,
          Language: 'en'
        },
        {
          stateContext: this,
          onSuccess: (json) => {
            this.setState({ loader: false }, () => {
              if (json.success) {
                toast.success('User Created');

                // this.props.updateUser(json.data);

                // Set UX improvement cookie
                Cookies.set(appSettings.cookieName, '1', { expires: 365 });

                onSuccess();
              } else {
                toast.error('error');
              }
            });
          },
          onError: (error) => {
            this.setState({ loader: false }, () => {
              toast.error(`${error}`);
            });
          }
        }
      );
    });
  };

  render() {
    const { loader, acceptTerms, gender } = this.state;

    return (
      <OnBoardingFormContainer title="Account aanmaken">
        <h6 className="section-title no-gutters">Persoonsgegevens</h6>

        <SplitInputRow label="Barnummer">
          <Input
            value={this.state.barNumber}
            onChange={(r) => this.setState({ barNumber: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Geboortedatum">
          <Input
            value={this.state.dateOfBirth}
            onChange={(r) => this.setState({ dateOfBirth: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Geslacht">
          <RadioGroup
            options={genderOptions}
            value={gender}
            horizontal
            onChange={(r) => this.setState({ gender: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Initialen">
          <Input
            value={this.state.initials}
            onChange={(r) => this.setState({ initials: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Voornaam">
          <Input
            value={this.state.firstName}
            onChange={(r) => this.setState({ firstName: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Tussenvoegsel">
          <Input
            value={this.state.middleName}
            onChange={(r) => this.setState({ middleName: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Achternaam">
          <Input
            value={this.state.lastName}
            onChange={(r) => this.setState({ lastName: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Telefoonnummer">
          <Input
            value={this.state.phone}
            onChange={(r) => this.setState({ phone: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="E-mail">
          <Input
            value={this.state.email}
            onChange={(r) => this.setState({ email: r })}
          />
        </SplitInputRow>

        <h6 className="section-title no-gutters">Wachtwoord</h6>

        <SplitInputRow label="Wachtwoord">
          <Input
            type="password"
            value={this.state.password}
            onChange={(r) => this.setState({ password: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Herhaal wachtwoord">
          <Input
            type="password"
            value={this.state.confirmPassword}
            onChange={(r) => this.setState({ confirmPassword: r })}
          />
        </SplitInputRow>

        <div className="row form-item">
          <div className="box-around">
            <div className="col-12">
              <Checkbox
                value={acceptTerms}
                onChange={(r) => this.setState({ acceptTerms: r })}
                label="Ik ga akkoord met de algemene voorwaarden en de privacyverklaring van legal.nl." />
            </div>
          </div>
        </div>

        <hr className="horizontal-rule" />

        <div className="row form-item justify-content-end">
          {loader ? (
            <Loader margin={false} />
          ) : (
            <Button onClick={this.signUp}>Volgende stap</Button>
          )}
        </div>
      </OnBoardingFormContainer>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.app.user
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(StepOneForm);
