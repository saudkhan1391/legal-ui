import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-responsive-modal';
import Cookies from 'js-cookie';

import Button from '@empire/hydra/components/Button';

class CookiePopup extends Component {
  static propTypes = {
    i18n: PropTypes.object.isRequired
  };

  state = {
    isOpen: Cookies.get('.LegalCookie') !== '1'
  };

  setCookie() {
    Cookies.set('.LegalCookie', '1', { expires: 10000 });

    this.setState({ isOpen: false });
  }

  render() {
    const { i18n } = this.props;
    const { isOpen } = this.state;

    return (
      <Modal
        classNames={{
          overlay: 'overlay',
          modal: 'cookie-popup js-modal'
        }}
        closeOnEsc={false}
        closeOnOverlayClick={false}
        showCloseIcon={false}
        open={isOpen}
        center
      >
        <h2>{i18n.t('cookies.title')}</h2>
        <p>{i18n.t('cookies.description')}</p>
        <Button
          appendIcon="fas fa-long-arrow-alt-right"
          onClick={() => this.setCookie()}
        >
          Okay
        </Button>
      </Modal>
    );
  }
}

// connect redux
const mapStateToProps = (state) => ({
  i18n: state.app.i18n
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CookiePopup);
