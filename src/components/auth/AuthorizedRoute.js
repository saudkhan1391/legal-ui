import React from 'react';
import Proptypes from 'prop-types';
import { connect } from 'react-redux';
import ProtectedRoute from '@empire/shared-ui/ProtectedRoute';
import { Redirect } from 'react-router-dom';
import { CONSTANTS } from 'lib/utils';

const AuthorizedRoute = ({
  isAuthenticated,
  user,
  path,
  component,
  redirectPrefix = '',
  cookieName,
  ...rest
}) => {
  if (!isAuthenticated) {
    return <Redirect to={`${redirectPrefix}/login`} />;
  }

  if (user.profileCompleted) {
    if (path.indexOf(CONSTANTS.GET_STARTED) !== -1) {
      return <Redirect to={`${redirectPrefix}/${CONSTANTS.DASHBOARD}`} />;
    }
  } else if (path.indexOf(CONSTANTS.GET_STARTED) === -1) {
    return <Redirect to={`${redirectPrefix}/${CONSTANTS.GET_STARTED}`} />;
  }
  return (
    <ProtectedRoute
      isAuthenticated={isAuthenticated}
      cookieName={cookieName}
      redirectPrefix={redirectPrefix}
      path={path}
      component={component}
      {...rest}
    />
  );
};

AuthorizedRoute.propTypes = {
  isAuthenticated: Proptypes.bool,
  user: Proptypes.object.isRequired,
  path: Proptypes.string.isRequired,
  component: Proptypes.object.isRequired,
  redirectPrefix: Proptypes.string,
  cookieName: Proptypes.string
};

AuthorizedRoute.defaultProps = {
  redirectPrefix: '',
  cookieName: null,
  isAuthenticated: false
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.app.isAuthenticated,
  user: state.app.user
});

export default connect(mapStateToProps, null)(AuthorizedRoute);
