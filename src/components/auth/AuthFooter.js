import React from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';

const AuthFooter = ({ i18n }) => (
  <div className="simple-footer row no-gutters">
    <div className="copyright col-12 col-sm-6">
      {i18n.t('shared.copyright', { year: moment().year() })}
    </div>

    <div className="links col-12 col-sm-6">
      <ul>
        <li>
          <Link to="/terms-of-service">{i18n.t('words.terms_of_service')}</Link>
        </li>
        <li>
          <Link to="/privacy-policy">{i18n.t('words.privacy')}</Link>
        </li>
        <li>
          <Link to="/faq">{i18n.t('faq')}</Link>
        </li>
      </ul>
    </div>
  </div>
);

AuthFooter.propTypes = {
  i18n: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
  i18n: state.app.i18n
});

export default connect(mapStateToProps)(AuthFooter);
