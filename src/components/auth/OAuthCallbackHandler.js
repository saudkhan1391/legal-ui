import React, { useEffect, useState } from 'react';
import { fetchGET } from '@empire/shared-ui/fetch';
import { appSettings } from 'settings';
import { updateUser } from '@empire/shared-ui/actions/appActions';
import { generatePostAuthRoute } from 'lib/utils';
import { toast } from 'react-toastify';
import { updateOAuthData } from 'lib/appActions';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

const OAuthCallbackHandler = ({
  i18n,
  match,
  updateUser: updateLoggedInUser,
  updateOAuthData: updateRegistrationData
}) => {
  const [redirectURL, setRedirectURL] = useState('');
  useEffect(() => {
    fetchGET(`${appSettings.apiBaseUrl}/account/external_login_status`, {
      onSuccess: ({ success, data, error }) => {
        if (success) {
          updateLoggedInUser(data);
          setRedirectURL(generatePostAuthRoute(data));
        } else {
          if (error) {
            toast.error(i18n.t(`errors.${error}`));
            return;
          }

          updateRegistrationData(data);
          let prefix = '';
          if (match.params && match.params.userType === 'business') {
            prefix = '/business';
          }
          setRedirectURL(`${prefix}/register`);
        }
      }
    });
  }, []);

  return redirectURL && <Redirect to={redirectURL} />;
};

const mapStateToProps = (state) => ({
  i18n: state.app.i18n
});

const mapDispatchToProps = {
  updateUser,
  updateOAuthData
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OAuthCallbackHandler);
