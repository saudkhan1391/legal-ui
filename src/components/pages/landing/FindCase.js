import React, { Component } from 'react';
import Button from '@empire/hydra/components/Button';

const images = {
  peopleOffice: require('assets/images/people-office.svg')
};

class FindCase extends Component {
  render() {
    return (
      <section className="find-case-section">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-6 title">
              <h4>De juiste advocaat vindt uw zaak</h4>
              <p>
                Wij bieden advocaten en rechtzoekenden een platform waarop zij
                elkaar kunnen vinden.
              </p>
              <Button>Plaats gratis uw dagvaarding</Button>
            </div>

            <div className="col-6 image">
              <img src={images.peopleOffice} alt="people-office" />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default FindCase;
