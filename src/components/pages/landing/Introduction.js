import React, { Component } from 'react';

import Button from '@empire/hydra/components/Button';
import Hero from 'components/layout/Hero';

class Introduction extends Component {
  render() {
    return (
      <Hero>
        <div className="container introduction-hero-section">
          <div className="row">
            <div className="col-12 col-lg-6 left-pane">
              <h4 className="dark">
                Gedagvaard en op zoek naar de juiste advocaat voor uw zaak?
              </h4>
              <p className="dark display-2 mt-6">
                De meest geschikte advocaten die u direct kunnen helpen,
                reageren op uw zaak, zodat u zo goed mogelijk geholpen wordt.
              </p>
              <div className="row">
                <Button>Plaats gratis uw dagvaarding</Button>
              </div>
            </div>
            <div className="col-12 col-md-6">
              <div className="browsing-people-img" />
            </div>
          </div>
        </div>
      </Hero>
    );
  }
}

export default Introduction;
