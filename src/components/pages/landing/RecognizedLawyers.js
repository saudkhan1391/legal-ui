import React, { Component } from 'react';
import Button from '@empire/hydra/components/Button';

const images = {
  peopleStanding: require('assets/images/people-standing.png')
};

class RecognizedLawyers extends Component {
  render() {
    return (
      <section className="recognized-lawyers-section">
        <div className="container-fluid no-padding">
          <div className="row">
            <div className="col-6 image">
              <img src={images.peopleStanding} alt="people-standing" />
            </div>
            <div className="col-12 col-md-6 content">
              <h4>Alleen erkende advocaten</h4>
              <p>
                Advocaten zijn lid van de Nederlandse Orde van Advocaten, met
                alle daaraan gekoppelde waarborgen, waaronder een
                geheimhoudingsplicht. Uitsluitend advocaten hebben toegang tot
                legal.nl.
              </p>
              <Button>Plaats gratis uw dagvaarding</Button>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default RecognizedLawyers;
