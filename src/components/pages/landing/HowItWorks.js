import React, { Component } from 'react';
import Button from '@empire/hydra/components/Button';

const images = {
  envelope: require('assets/images/envelope.svg'),
  trophies: require('assets/images/trophies.svg'),
  likeDislikes: require('assets/images/like-dislikes.svg')
};

class HowItWorks extends Component {
  render() {
    return (
      <section className="how-it-works-section">
        <div className="container">
          <div className="col-12 title">
            <h6 className="grey">Zo makkelijk werkt legal.nl</h6>
            <h4>Hoe werkt het?</h4>
          </div>
          <div className="detail">
            <div className="row">
              <div className="col-12 col-md-4">
                <div className="item-detail">
                  <img
                    className="image-sqr"
                    src={images.envelope}
                    alt="envelope"
                  />
                  <h6 className="heading">Plaats uw dagvaarding</h6>
                  <p className="paragraph">
                    Scan uw dagvaarding en plaats die op de site. Alleen
                    advocaten kunnen uw dagvaarding inzien.
                  </p>
                </div>
              </div>
              <div className="col-12 col-md-4">
                <div className="item-detail">
                  <img
                    className="image-sqr"
                    src={images.trophies}
                    alt="trophies"
                  />
                  <h6 className="heading">Advocaten melden zich</h6>
                  <p className="paragraph">
                    Advocaten die u direct en goed kunnen ondersteunen
                    introduceren zich bij u.
                  </p>
                </div>
              </div>
              <div className="col-12 col-md-4">
                <div className="item-detail">
                  <img
                    className="image-sqr"
                    src={images.likeDislikes}
                    alt="likes"
                  />
                  <h6 className="heading">U beslist</h6>
                  <p className="paragraph">
                    Leg gratis contact met de advocaat van uw keuze.
                  </p>
                </div>
              </div>
              <div className="col-12 activate-button">
                <Button>Plaats gratis uw dagvaarding</Button>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default HowItWorks;
