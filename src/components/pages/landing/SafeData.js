import React, { Component } from 'react';
import Button from '@empire/hydra/components/Button';

const images = {
  servers: require('assets/images/servers.png')
};

class SafeData extends Component {
  render() {
    return (
      <section className="safe-data-section">
        <div className="container-fluid no-padding">
          <div className="row">
            <div className="col-12 col-md-6 content">
              <h4>Uw gegevens zijn veilig</h4>
              <p>
                Wij waarborgen uw privacy volledig volgens de actuele wetgeving
                en delen uw gegevens nooit met derden.
              </p>
              <Button>Plaats gratis uw dagvaarding</Button>
            </div>
            <div className="col-6 image">
              <img src={images.servers} alt="people-standing" />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default SafeData;
