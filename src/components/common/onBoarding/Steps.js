import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Steps extends Component {
  static propTypes = {
    steps: PropTypes.shape({
      title: PropTypes.string,
      isActive: PropTypes.bool,
      stepNumber: PropTypes.number
    }).isRequired
  };

  renderSteps = () => {
    const { steps } = this.props;

    if (Object.keys(steps).length) {
      return steps.map((step) => (
        <div
          key={step.stepNumber}
          className={`row item ${step.isActive ? '' : 'in-active'}`}
          >
          <h6 className="number-circle">{step.stepNumber}</h6>
          <h6>{step.title}</h6>
        </div>
      ));
    }

    return null;
  };

  render() {
    return (
      <section className="client-onboarding-steps">
        {this.renderSteps()}

        <p className="message">
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
          dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est
          laborum sunt in culpa qui officia sint occaecat.
        </p>

        <div className="contact">
          <h6>Vragen?</h6>
          <p>
            Bel ons op 020 123 456 7 <br /> of mail met frans@legal.nl
          </p>
        </div>
      </section>
    );
  }
}

export default Steps;
