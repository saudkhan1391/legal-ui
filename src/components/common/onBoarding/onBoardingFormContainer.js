import React, { Component } from 'react';
import PropTypes from 'prop-types';

class OnBoardingFormContainer extends Component {
  render() {
    const { title, customStyle } = this.props;

    return (
      <div className={`${customStyle} onboarding-form-container`}>
        {title.length > 0 && (
          <>
            <h5>{title}</h5>
            <hr className="horizontal-rule" />
          </>
        )}

        {this.props.children}
      </div>
    );
  }
}

OnBoardingFormContainer.propTypes = {
  title: PropTypes.string,
  customStyle: PropTypes.string
};

OnBoardingFormContainer.defaultProps = {
  title: '',
  customStyle: ''
};

export default OnBoardingFormContainer;
