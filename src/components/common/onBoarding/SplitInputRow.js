import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SplitInputRow extends Component {
  render() {
    const { label, hint } = this.props;

    return (
      <div className="row split-input-row">
        <div className="col-12 col-lg-6 left">
          <span className="label display-2">{label}</span>
          <br />
          {hint && <span className="label hint display-2">{hint}</span>}
        </div>
        <div className="col-12 col-lg-6 no-padding right">
          {this.props.children}
        </div>
      </div>
    );
  }
}

SplitInputRow.propTypes = {
  label: PropTypes.string.isRequired,
  hint: PropTypes.string
};

SplitInputRow.defualtProps = {
  hint: null
};

export default SplitInputRow;
