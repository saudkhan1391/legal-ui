import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Spacer from '@empire/hydra/components/Spacer';
import Button from '@empire/hydra/components/Button';

const images = {
  checkCircle: require('assets/images/check-circle.svg')
};

class InactiveStepBox extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    stepNumber: PropTypes.number.isRequired,
    isCompleted: PropTypes.bool.isRequired,
    onEdit: PropTypes.func
  };

  static defaultProps = {
    onEdit: () => {}
  };

  render() {
    const { title, stepNumber, isCompleted, onEdit } = this.props;
    return (
      <div className="form-in-active">
        {isCompleted && (
          <img
            className="icon d-none d-sm-block"
            src={images.checkCircle}
            alt="check-circle"
          />
        )}
        <h5>{title}</h5>
        {isCompleted && (
          <>
            <Spacer />
            <Button onClick={() => onEdit(stepNumber)}>Wijzigen</Button>
          </>
        )}
      </div>
    );
  }
}

export default InactiveStepBox;
