import React, { Component } from 'react';

class Hero extends Component {
  render() {
    return (
      <div className="hero-section">
        <div className="background-container">{this.props.children}</div>
      </div>
    );
  }
}

export default Hero;
