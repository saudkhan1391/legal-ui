import React, { Component } from 'react';
import Button from '@empire/hydra/components/Button';
import Spacer from '@empire/hydra/components/Spacer';

const images = {
  logo: require('assets/images/logo.svg')
};

class Footer extends Component {
  render() {
    return (
      <div className="footer-container">
        <div className="background-container">
          <div className="container body">
            <div className="row">
              <div className="col-6">
                <h5 className="dark">Start nu!</h5>
                <div className="row">
                  <Button>Plaats gratis uw dagvaarding</Button>
                </div>
              </div>
              <div className="col-6 right">
                <div className="row">
                  <div className="col-12">
                    <h5 className="dark">Ontvang tips in je mailbox!</h5>
                    <div className="row">
                      <Button>Plaats gratis uw dagvaarding</Button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12">
                <hr />
                <p className="dark mt-6">
                  Legal.nl is een activiteit van Legal & Fiscal BV (KvK
                  75571072) en is gevestigd in Kasteel Moersbergen aan de
                  Moersbergselaan 17 te 3941 BW Doorn. Het postadres is Postbus
                  186, 3940 AD Doorn. Contactadres: info@legal.nl.
                </p>
              </div>
              <div className="col-12">
                <div className="row no-gutters logo-row">
                  <img src={images.logo} alt="logo" />
                  <Spacer />
                  <div className="row footer-mid">
                    <p className="dark mr-4">Algemene voorwaarden</p>

                    <p className="dark mr-4">Privacyverklaring</p>

                    <p className="dark">Disclaimer</p>
                    <div className="col-12">
                      <p className="dark">
                        © Alle rechten voorbehouden 2020 - Legal & Fiscal B.V.
                      </p>
                    </div>
                  </div>
                  <Spacer />
                  <p className="dark">Volg ons op</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
