import React, { Component } from 'react';

class LinkButton extends Component {
  render() {
    const { children } = this.props;

    return (
      <button
        className={`link-button ${this.props.className}`}
        type="button"
        onClick={() => this.props.onClick()}
      >
        {children}
      </button>
    );
  }
}

export default LinkButton;
