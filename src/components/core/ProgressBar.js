import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ProgressBar extends Component {
  static propTypes = {
    progress: PropTypes.number.isRequired,
    withLabel: PropTypes.bool,
    color: PropTypes.string,
    height: PropTypes.number
  };

  static defaultProps = {
    withLabel: false,
    color: '1FCE99',
    height: 5
  };

  componentDidMount() {
    this.setProgress();
  }

  setProgress() {
    const { progress, withLabel } = this.props;

    const elem = document.getElementById('progress-bar');
    const width = progress;

    elem.style.width = `${width}%`;

    if (withLabel) {
      elem.innerHTML = `${width}%`;
    }
  }

  render() {
    const { progress, withLabel, color, height } = this.props;

    return (
      <div id="progress-bar-container" style={{ border: `1px solid ${color}` }}>
        <div
          id="progress-bar"
          style={{ backgroundColor: `${color}`, height: `${height}px` }}
        >
          {withLabel && progress}
        </div>
      </div>
    );
  }
}

ProgressBar.propTypes = {};

export default ProgressBar;
