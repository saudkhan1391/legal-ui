import React, { Component } from 'react';

class AuthFooter extends Component {
  static propTypes = {};

  static defaultProps = {};

  render() {
    return (
      <div className="row no-gutters auth-footer">
        <div className="col-12 col-md-6">
          <label>© Alle rechten voorbehouden 2019 - Legal & Fiscal B.V.</label>
        </div>
        <div className="col-12 col-md-6">
          <ul>
            <li>
              <a>Algemene voorwaarden</a>
            </li>
            <li>
              <a>Privacy</a>
            </li>
            <li>
              <a>Help</a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default AuthFooter;
