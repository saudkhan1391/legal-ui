import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '@empire/hydra/components/Button';

class ArrowButton extends Component {
  static propTypes = {
    label: PropTypes.string,
    iconName: PropTypes.string,
    onClick: PropTypes.func
  };

  static defaultProps = {
    iconName: 'fas fa-long-arrow-alt-right'
  };

  render() {
    const { label, iconName, onClick } = this.props;

    return (
      <Button onClick={() => onClick()}>
        {label} <i className={`${iconName}`} />
      </Button>
    );
  }
}

ArrowButton.propTypes = {};

export default ArrowButton;
