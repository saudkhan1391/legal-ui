/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import Dropzone from 'react-dropzone';
import Iframe from 'react-iframe';

import Button from '@empire/hydra/components/Button';
import Loader from '@empire/hydra/components/Loader';
import Modal from '@empire/hydra/components/Modal';
import { fetchPOST, postUpload, fetchGET } from '@empire/shared-ui/fetch';

import OnBoardingFormContainer from 'components/common/onBoarding/onBoardingFormContainer';

import { appSettings } from 'components/../settings';

class StepThreeForm extends Component {
  static propTypes = {
    onSuccess: PropTypes.func,
    user: PropTypes.object.isRequired,
  };

  static defaultProps = {
    onSuccess: () => {}
  };

  state = {
    modalValue: [],
    modalTitle: '',
    selectedURL: '',
    selectedModalIndex: null,
    fileName: [],
    fileUrl: [],
    removeUrlKey: [],
    loader: false
  }

  componentDidMount = () => {
    const { user } = this.props;

    this.setState({ loader: true }, () => {
      fetchPOST(
        `${appSettings.apiBaseUrl}/document/Index`,
        {
          key: user.caseKey
        },
        {
          stateContext: this,
          onSuccess: (json) => {
            this.setState({ loader: false }, () => {
              if (json.success) {
                this.setState({ fileUrl: json.data });
              }
            });
          },
          onError: (error) => {
            this.setState({ loader: false }, () => {
              toast.error(`errors.${error}`);
            });
          }
        }
      );
    });
  }

  uploadFile = () => {
    const { user } = this.props;
    const { fileName, fileUrl } = this.state;
    const data = new FormData();

    if (fileName.length !== 0) {
      this.setState({ loader: true }, () => {
        fileName.map((file) => (
          data.append('Files', file)
        ));
        data.append('Key', user.caseKey);
        postUpload(`${appSettings.apiBaseUrl}/document/upload`, data, {
          stateContext: this,
          onSuccess: (json) => {
            if (json.success) {
              toast.success('File uploaded');

              this.setState({ fileName: [], loader: false });

              this.DeleteFile();
            }
          },
          onError: (error) => {
            this.setState({ loader: false }, () => {
              toast.error(error);
            });
          }
        });
      });
    } else if (fileUrl.length !== 0) {
      this.DeleteFile();
    } else {
      toast.error('Please Select File');
    }
  }

  DeleteFile = () => {
    const { onSuccess } = this.props;
    const { removeUrlKey } = this.state;
    if (removeUrlKey.length > 0) {
      (removeUrlKey.map((key) => (
        fetchGET(`${appSettings.apiBaseUrl}/document/delete/${key}`, {
          stateContext: this,
          onSuccess: (json) => {
            this.setState({ loader: false }, () => {
              if (json.success) {
                this.setState({
                  removeUrlKey: []
                }, () => {
                  toast.success('File Deleted');
                });
              } else {
                toast.error('error');
              }
            });
          },
          onError: (error) => {
            this.setState({ loader: false }, () => {
              toast.error(`${error}`);
            });
          }
        })
      )));
    } else {
      onSuccess();
    }
  }

  removeFile(remove, key) {
    const { fileName, fileUrl, removeUrlKey } = this.state;
    if (key !== 'url') {
      this.setState({ fileName: fileName.filter((file) => file !== remove) });
    } else {
      this.setState({ fileUrl: fileUrl.filter((file) => file !== remove), removeUrlKey: [...removeUrlKey, remove.key] });
    }
  }

  openModal = (url, index) => {
    const { modalValue } = this.state;
    const modalNewValue = modalValue.slice();
    modalNewValue[index] = true;
    this.setState({ modalValue: modalNewValue, selectedURL: url, selectedModalIndex: index });
  }

  randerFiles = () => {
    const { modalValue, modalTitle, selectedURL, selectedModalIndex } = this.state;
    return (
      <Modal
        show={modalValue[selectedModalIndex] || false}
        title={modalTitle}
        onClose={() => this.setState({ modalValue: [false] })}
      >
        <Iframe
          url={selectedURL}
          width="100%"
          height="500px"
          id="myId"
          className="iframe"
          display="inline"
          frameBorder="0"
          position="relative"
          loading="loading" />
      </Modal>
    );
  }

  render() {
    const { fileName, loader, fileUrl } = this.state;

    return (
      <OnBoardingFormContainer title="Dagvaarding of verzoekschrift plaatsen">
        <div className="upload-document-box">
          <div className="col-12">
            <p className="display-2">
              <Dropzone accept="image/jpeg, image/png, application/pdf" onDrop={(acceptedFiles) => this.setState({ fileName: acceptedFiles })}>
                {({ getRootProps, getInputProps }) => (
                  <section>
                    <div className="d-flex align-items-center" {...getRootProps()}>
                      <input {...getInputProps()} />
                      <i className="fa fa-paperclip icon" />
                      <button className="btn secondary-button" type="button">Plaats uw dagvaarding of verzoekschrift (PDF of JPEG bestanden)</button>
                    </div>
                  </section>
                )}
              </Dropzone>
            </p>
          </div>
        </div>

        <div className="upload-progress">

          {fileName.length > 0
          && (
            fileName.map((file) => (
              <p className="file-title display-2">{file.name}
                <span className="close-icon ml-3" onClick={() => this.removeFile(file)}>
                  <i className="fa fa-times-circle" />
                </span>
              </p>
            ))
          )}

          {fileUrl.length > 0
          && (
            fileUrl.map((file, index) => (
              <p className="file-title display-2">
                <lable onClick={() => this.openModal(file.url, index)}>{file.url}</lable>
                <span className="close-icon ml-3" onClick={() => this.removeFile(file, 'url')}>
                  <i className="fa fa-times-circle" />
                </span>
              </p>
            ))
          )}

          {this.randerFiles()}
        </div>

        <hr className="horizontal-rule" />

        <div className="row form-item justify-content-end">
          {loader ? (
            <Loader margin={false} />
          ) : (
            <Button onClick={this.uploadFile}>Volgende stap</Button>
          )}
        </div>
      </OnBoardingFormContainer>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.app.user
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(StepThreeForm);
