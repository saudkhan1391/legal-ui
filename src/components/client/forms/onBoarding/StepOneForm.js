import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import Cookies from 'js-cookie';

import RadioGroup from '@empire/hydra/components/RadioGroup';
import Input from '@empire/hydra/components/Input';
import Checkbox from '@empire/hydra/components/Checkbox';
import Button from '@empire/hydra/components/Button';
import Loader from '@empire/hydra/components/Loader';
import { fetchPOST } from '@empire/shared-ui/fetch';

import { updateUser } from '@empire/shared-ui/actions/appActions';

import SplitInputRow from 'components/common/onBoarding/SplitInputRow';
import OnBoardingFormContainer from 'components/common/onBoarding/onBoardingFormContainer';

import { appSettings } from 'settings';

const genderOptions = [
  { label: 'Man', value: 'male' },
  { label: 'Vrouw', value: 'female' }
];

class StepOneForm extends Component {
  static propTypes = {
    onSuccess: PropTypes.func,
    user: PropTypes.object.isRequired,
    updateUser: PropTypes.func.isRequired,
  };

  static defaultProps = {
    onSuccess: () => {}
  };

  state = {
    gender: null,
    firstName: '',
    middleName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: '',
    phone: '',
    acceptTerms: false,
    loader: false
  };

  componentDidMount() {
    const { user, onSuccess } = this.props;
    if (user !== null) {
      onSuccess();
    }
  }

  signUp = () => {
    const { onSuccess } = this.props;

    const {
      email,
      lastName,
      middleName,
      firstName,
      gender,
      password,
      phone,
      acceptTerms,
      loader
    } = this.state;

    this.setState({ loader: !loader }, () => {
      fetchPOST(
        `${appSettings.apiBaseUrl}/account/register`,
        {
          acceptTerms,
          email,
          lastName,
          middleName,
          firstName,
          gender: gender ? 'Male' : 'Female',
          phone,
          password,
          UserType: 10,
          Language: 'en'
        },
        {
          stateContext: this,
          onSuccess: (json) => {
            console.log('sanan: ', json);
            this.setState({ loader: false }, () => {
              if (json.success) {
                toast.success('User Created');

                this.props.updateUser(json.data);

                // Set UX improvement cookie
                Cookies.set(appSettings.cookieName, '1', { expires: 365 });

                onSuccess();
              } else {
                toast.error('error');
              }
            });
          },
          onError: (error) => {
            this.setState({ loader: false }, () => {
              toast.error(`${error}`);
            });
          }
        }
      );
    });
  };

  render() {
    const { loader, acceptTerms, gender } = this.state;

    return (
      <OnBoardingFormContainer title="Account aanmaken">
        <h6 className="section-title no-gutters">Persoonsgegevens</h6>

        <SplitInputRow label="Geslacht">
          <RadioGroup
            options={genderOptions}
            horizontal
            value={gender}
            onChange={(r) => this.setState({ gender: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Voornaam">
          <Input
            value={this.state.firstName}
            onChange={(r) => this.setState({ firstName: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Tussenvoegsel">
          <Input
            value={this.state.middleName}
            onChange={(r) => this.setState({ middleName: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Achternaam">
          <Input
            value={this.state.lastName}
            onChange={(r) => this.setState({ lastName: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Telefoon">
          <Input
            value={this.state.phone}
            onChange={(r) => this.setState({ phone: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="E-mail">
          <Input
            value={this.state.email}
            onChange={(r) => this.setState({ email: r })}
          />
        </SplitInputRow>

        <h6 className="section-title no-gutters">Wachtwoord</h6>

        <SplitInputRow label="Wachtwoord">
          <Input
            type="password"
            value={this.state.password}
            onChange={(r) => this.setState({ password: r })}
          />
        </SplitInputRow>

        <SplitInputRow label="Herhaal wachtwoord">
          <Input
            type="password"
            value={this.state.confirmPassword}
            onChange={(r) => this.setState({ confirmPassword: r })}
          />
        </SplitInputRow>

        <div className="row form-item">
          <div className="box-around">
            <div className="col-12">
              <Checkbox
                value={acceptTerms}
                onChange={(r) => this.setState({ acceptTerms: r })}
                label="Ik ga akkoord met de algemene voorwaarden en de privacyverklaring van legal.nl." />
            </div>
          </div>
        </div>

        <hr className="horizontal-rule" />

        <div className="row form-item justify-content-end">
          {loader ? (
            <Loader margin={false} />
          ) : (
            <Button onClick={this.signUp}>Volgende stap</Button>
          )}
        </div>
      </OnBoardingFormContainer>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.app.user
});

const mapDispatchToProps = {
  updateUser
};

export default connect(mapStateToProps, mapDispatchToProps)(StepOneForm);
