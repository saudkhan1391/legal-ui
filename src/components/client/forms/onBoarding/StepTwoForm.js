import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import { fetchGET, fetchPOST } from '@empire/shared-ui/fetch';

import Input from '@empire/hydra/components/Input';
import RadioGroup from '@empire/hydra/components/RadioGroup';
import Popup from '@empire/hydra/components/Popup';
import CheckboxGroup from '@empire/hydra/components/CheckboxGroup';
import Button from '@empire/hydra/components/Button';
import Loader from '@empire/hydra/components/Loader';

import SplitInputRow from 'components/common/onBoarding/SplitInputRow';
import OnBoardingFormContainer from 'components/common/onBoarding/onBoardingFormContainer';

import { appSettings } from 'settings';

const options = [
  { label: 'Ja', value: true },
  { label: 'Nee', value: false }
];

class StepTwoForm extends Component {
  static propTypes = {
    onSuccess: PropTypes.func
  };

  static defaultProps = {
    onSuccess: () => {}
  };

  state = {
    popupOpen: false,
    searchValue: '',
    CheckboxOptions: [],
    expenseInsurance: 'false',
    languages: [],
    language: 'false',
    legalAid: 'false',
    loader: false
  };

  componentDidMount() {
    fetchGET(`${appSettings.apiBaseUrl}/client/on_boarding_data`, {
      stateContext: this,
      onSuccess: (json) => {
        this.setState({ loader: false }, () => {
          if (json.success) {
            this.setState({
              expenseInsurance: `${json.data.clientPreferenceRequest.expenseInsurance}`,
              legalAid: `${json.data.clientPreferenceRequest.legalAid}`,
              languages: json.data.clientPreferenceRequest.languages,
              language: json.data.clientPreferenceRequest.languages.length !== 0 ? 'true' : 'false'
            });
          } else {
            toast.error('error');
          }
        });
      },
      onError: (error) => {
        this.setState({ loader: false }, () => {
          toast.error(`${error}`);
        });
      }
    });
  }

  loadLanguages() {
    fetchGET(`${appSettings.apiBaseUrl}/service/languages`, {
      stateContext: this,
      onSuccess: (json) => {
        this.setState({ loader: false }, () => {
          if (json.success) {
            this.setState({
              CheckboxOptions: json.data
            });
          } else {
            toast.error('error');
          }
        });
      },
      onError: (error) => {
        this.setState({ loader: false }, () => {
          toast.error(`${error}`);
        });
      }
    });
  }

  convertToBool = (value) => {
    const val = (value === 'true');
    return val;
  }

  sendPreferences = () => {
    const { onSuccess } = this.props;
    const { expenseInsurance, languages, legalAid } = this.state;
    const expenseBool = this.convertToBool(expenseInsurance);
    const legalBool = this.convertToBool(legalAid);

    this.setState({ loader: true }, () => {
      fetchPOST(
        `${appSettings.apiBaseUrl}/client/preferences`,
        {
          expenseInsurance: expenseBool,
          legalAid: legalBool,
          languages
        },
        {
          stateContext: this,
          onSuccess: (json) => {
            this.setState({ loader: false }, () => {
              if (json.success) {
                onSuccess();
              }
            });
          },
          onError: (error) => {
            this.setState({ loader: false }, () => {
              toast.error(`errors.${error}`);
            });
          }
        }
      );
    });
  };

  render() {
    const { popupOpen, CheckboxOptions, expenseInsurance, languages, language, legalAid, loader } = this.state;

    return (
      <OnBoardingFormContainer
        title="Aanvullende vragen"
        customStyle="custom-radio-box"
      >
        <SplitInputRow label="Heeft u een rechtsbijstandverzekering">
          <RadioGroup
            options={options}
            horizontal
            value={expenseInsurance}
            onChange={(r) => this.setState({ expenseInsurance: r })}
            required
          />
        </SplitInputRow>

        <SplitInputRow
          label="Heeft u recht op gesubsidieerde rechtsbijstand?"
          hint="(heeft u recht op een toevoeging?)"
        >
          <RadioGroup
            options={options}
            horizontal
            value={legalAid}
            onChange={(r) => this.setState({ legalAid: r })}
            required
          />
        </SplitInputRow>

        <SplitInputRow label="Heeft u een voorkeurstaal die anders is dan Nederlands?">
          <Popup manualControl isOpen={popupOpen}>
            <Popup.Activator>
              <RadioGroup
                options={options}
                horizontal
                required
                value={language}
                onChange={(r) => {
                  this.setState({ language: r });
                  const val = (r === 'true');
                  if (val) {
                    this.setState({ popupOpen: true }, () => {
                      this.loadLanguages();
                    });
                  } else {
                    this.setState({ popupOpen: false, languages: [1] });
                  }
                }}
              />
            </Popup.Activator>
            <Popup.Content>
              <div className="row no-gutters">
                <div className="col-12">
                  <Input
                    value={this.state.searchValue}
                    onChange={(r) => console.log(r)}
                  />
                </div>
                <div className="col-12 d-flex justify-content-between mt-3">
                  <label>SELECTEER ALLE</label>
                  <a>RESET</a>
                </div>
                <div className="col-12">
                  <CheckboxGroup
                    dataText="name"
                    dataValue="id"
                    value={languages}
                    options={CheckboxOptions}
                    onChange={(r) => this.setState({ languages: r })}
                  />
                </div>
                <hr className="horizontal-rule" />

                <div className="col-12">
                  <Button
                    appendIcon="fas fa-long-arrow-alt-right"
                    onClick={() => {
                      if (languages.length !== 0) {
                        this.setState({ popupOpen: false });
                      }
                    }}
                  >
                    Toevoegen
                  </Button>
                </div>
              </div>
            </Popup.Content>
          </Popup>
        </SplitInputRow>

        <hr className="horizontal-rule" />

        <div className="row form-item justify-content-end">
          {loader ? (
            <Loader margin={false} />
          ) : (
            <Button
              appendIcon="fas fa-long-arrow-alt-right"
              onClick={this.sendPreferences}
            >
              Volgende stap
            </Button>
          )}
        </div>
      </OnBoardingFormContainer>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(StepTwoForm);
