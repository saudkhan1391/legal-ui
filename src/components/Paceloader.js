import React, { Component } from 'react';

export default class PaceLoader extends Component {
  render() {
    return (
      <div id="paceloader" className="paceloader-container">
        <div className="paceloader">
          <div className="paceloader-inner">
            <div />
          </div>
        </div>
      </div>
    );
  }
}
