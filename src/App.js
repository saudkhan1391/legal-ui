import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

import RehydrateStore from '@empire/shared-ui/components/RehydrateStore';
import Error from '@empire/shared-ui/components/Error';
import { updateUser, logoutUser } from '@empire/shared-ui/actions/appActions';

import { appSettings } from './settings';

import withNonFluidLayout from './pages/layouts/NonFluidLayout';
import withMasterLayout from './pages/layouts/MasterLayout';

// CookiePopup Modal
// import CookiePopup from './components/CookiePopup';
import PaceLoader from './components/Paceloader';

import LandingPage from './pages/Landing';

// Auth
import AuthorizedRoute from './components/auth/AuthorizedRoute';

// Client
import ClientOnBoarding from './pages/client/OnBoarding';
import Login from './pages/client/Login';
import Dashboard from './pages/client/Dashboard';

// Advocate
import AdvocateOnBoarding from './pages/advocate/OnBoarding';
import AdvocateLogin from './pages/advocate/AdvocateLogin';
import AdvocateDashboard from './pages/advocate/Dashboard';

const { cookieName } = appSettings;

class App extends Component {
  static propTypes = {
    updateUser: PropTypes.func.isRequired,
    logoutUser: PropTypes.func.isRequired
  };

  render() {
    return (
      <>
        <ToastContainer key="toasts" autoClose={3500} hideProgressBar />

        <RehydrateStore
          url={`${appSettings.apiBaseUrl}/account/me`}
          onSuccess={(res) => this.props.updateUser(res.data)}
          onError={(e) => {
            this.props.logoutUser();
            toast.error(e);
          }}
          cookieName={appSettings.cookieName}
          loading={() => <PaceLoader />}
        >
          <Router>
            {/* <CookiePopup /> */}
            <Switch>
              {/* Client */}
              <Route path="/login" component={Login} exact />
              <Route
                path="/on-boarding"
                component={withNonFluidLayout(ClientOnBoarding)}
              />
              <AuthorizedRoute
                cookieName={cookieName}
                path="/dashboard"
                component={withNonFluidLayout(Dashboard)}
                exact
              />

              {/* Advocate */}
              {/* <AuthorizedRoute
                cookieName={cookieName}
                redirectPrefix="/advocate"
                path="/advocate/on-boarding"
                component={withNonFluidLayout(AdvocateOnBoarding)}
                exact /> */}
              <Route path="/advocate/login" component={AdvocateLogin} />
              <Route
                path="/advocate/on-boarding"
                component={withNonFluidLayout(AdvocateOnBoarding)}
              />
              <Route
                path="/advocate/dashboard"
                component={withNonFluidLayout(AdvocateDashboard)}
              />
              <Route path="/" component={withMasterLayout(LandingPage)} />
              <Error
                title="Not Found"
                message="The page you have requested can not be found."
                notFound
              />
            </Switch>
          </Router>
        </RehydrateStore>
      </>
    );
  }
}

// connect redux
const mapStateToProps = (state) => ({
  user: state.app.user,
  notifications: state.notifications,
  isAuthenticated: state.app.isAuthenticated
});

const mapDispatchToProps = {
  updateUser,
  logoutUser
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
