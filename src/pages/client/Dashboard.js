import React, { Component } from 'react';

import Button from '@empire/hydra/components/Button';
import Modal from '@empire/hydra/components/Modal';
import Checkbox from '@empire/hydra/components/Checkbox';

import IconButton from 'components/core/IconButton';

const images = {
  dummyMap: require('assets/images/dummy-map.svg')
};

class Dashboard extends Component {
  state = {
    list: [
      {
        title: 'Pieter Boemel'
      },
      {
        title: 'Pieter Boemel'
      },
      {
        title: 'Pieter Boemel'
      },
      {
        title: 'Pieter Boemel'
      }
    ],
    modalValue: false
  };

  renderList() {
    const { list, modalValue } = this.state;

    return (
      <>
        {list.map((item, i) => (
          <div key={i} className="list-box">
            <p>
              Ik kan je helpen, afgelopen 5 jaar ben koning geworden op dit
              gebied. Duis aute irure dolor in reprehenderit in voluptate velit
              esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
              occaecat cupidatat non proident, sunt in culpa qui officia
              deserunt mollit anim laborum sunt in culpa qui officia sint
              occaecat cupidatat. Duis aute irure dolor in reprehenderit in
              voluptate velit esse cillum dolore eu fugiat nulla pariatur.
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit.
            </p>
            <div className="list-action">
              <div className="list-detail">
                <label className="list-name">
                  {item.title} <span>Partner & Partners N.V.</span>
                </label>
              </div>
              <Button
                onClick={() => this.setState({
                  modalValue: !modalValue
                })}
              >
                Match met deze advocaat
              </Button>
            </div>
          </div>
        ))}
      </>
    );
  }

  render() {
    const { modalValue } = this.state;

    return (
      <div className="page dashboard">
        <div className="row no-gutters">
          <div className="col-12">
            <div className="box">
              <div className="row">
                <div className="col-8">
                  <div className="title">
                    <h5>Wij hebben 3 matches voor je John!</h5>
                  </div>
                  {this.renderList()}
                </div>
                <div className="col-4">
                  {/* <img src={images.dummyMap} width="100%" alt="logo" /> */}
                </div>
              </div>
            </div>
          </div>

          <Modal
            show={modalValue}
            title="Veel succes!"
            onClose={() => this.setState({
              modalValue: false
            })}
          >
            <div className="row">
              <div className="col-12">
                <hr className="mt-0" />

                <div className="detail">
                  <label>Pieter Partners</label>

                  <label>pieterlorem@partners.nl</label>

                  <label>020 1234567</label>

                  <label className="my-4">
                    De advocaat zal binnen enkele uren contact met u opnemen.
                  </label>

                  <label>
                    Als u nog vragen heeft, neem gerust contact op met Frans.
                    Hij is te bereiken op 0800 404040.
                  </label>
                </div>

                <Checkbox label="Mag Frans je binnenkort bellen hoe het is gegaan?" />

                <hr />

                <IconButton
                  label="Terug naar overzicht"
                  onClick={() => console.log('hello')}
                />
              </div>
            </div>
          </Modal>
        </div>
      </div>
    );
  }
}

export default Dashboard;
