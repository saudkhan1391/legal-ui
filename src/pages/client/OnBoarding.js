import React, { Component } from 'react';

import Steps from 'components/common/onBoarding/Steps';
import InactiveStepBox from 'components/common/onBoarding/InactiveStepBox';
import StepOneForm from 'components/client/forms/onBoarding/StepOneForm';
import StepTwoForm from 'components/client/forms/onBoarding/StepTwoForm';
import StepThreeForm from 'components/client/forms/onBoarding/StepThreeForm';
import StepFourForm from 'components/client/forms/onBoarding/StepFourForm';

class OnBoarding extends Component {
  state = {
    steps: [
      {
        title: 'Account aanmaken',
        stepNumber: 1,
        isActive: true,
        isCompleted: false,
        isLastStep: false
      },
      {
        title: 'Aanvullende vragen',
        stepNumber: 2,
        isActive: false,
        isCompleted: false,
        isLastStep: false
      },
      {
        title: 'Dagvaarding plaatsen',
        stepNumber: 3,
        isActive: false,
        isCompleted: false,
        isLastStep: false
      },
      {
        title: 'Match met advocaat',
        stepNumber: 4,
        isActive: false,
        isCompleted: false,
        isLastStep: true
      }
    ]
  };

  renderForm = () => {
    const { steps } = this.state;
    const formComponents = [
      {
        stepNumber: 1,
        component: <StepOneForm onSuccess={() => this.jumpToNextStep()} />
      },
      {
        stepNumber: 2,
        component: <StepTwoForm onSuccess={() => this.jumpToNextStep()} />
      },
      {
        stepNumber: 3,
        component: <StepThreeForm onSuccess={() => this.jumpToNextStep()} />
      },
      {
        stepNumber: 4,
        component: <StepFourForm />
      }
    ];

    const activeStep = steps.find((step) => step.isActive);

    return formComponents.find((fc) => fc.stepNumber === activeStep.stepNumber)
      .component;
  };

  renderFormBoxes = () => {
    const { steps } = this.state;

    return steps.map((step) => (
      <div key={step.stepNumber} className=" bordered-box">
        {!step.isActive && (
        <InactiveStepBox
          stepNumber={step.stepNumber}
          title={step.title}
          isCompleted={step.isCompleted}
          onEdit={(stepNumber) => this.jumpToStep(stepNumber)}
            />
        )}
        {step.isActive && (
        <div className="form-active">{this.renderForm()}</div>
        )}
      </div>
    ));
  };

  jumpToNextStep = () => {
    const { steps } = this.state;
    const stepsClone = steps;

    // to capture only the step next to previous active step
    let flag = false;

    stepsClone.map((step) => {
      if (step.isActive && !flag) {
        step.isActive = false;
        step.isCompleted = true;
        flag = true;
        return null;
      }
      if (flag) {
        step.isActive = true;
        flag = false;
      }
    });

    this.setState({ steps: stepsClone });
  };

  jumpToStep = (stepNumber) => {
    const { steps } = this.state;
    const stepsClone = steps;

    stepsClone.map((step) => {
      if (step.stepNumber === stepNumber) {
        step.isActive = true;
        step.isCompleted = false;
        return null;
      }
      step.isActive = false;
    });

    this.setState({ steps: stepsClone });
    return false;
  };

  render() {
    const { steps } = this.state;

    return (
      <div className="row on-boarding-page">
        <div className="col-12 heading">
          <h3>Welkom!</h3>
          <p>
            Wij gaan de eerste stap met je doornemen maecenas consequat iaculis
            velit sed.
          </p>
        </div>
        <div className="d-none d-lg-block col-lg-4">
          <div className="bordered-box">
            <Steps steps={steps} />
          </div>
        </div>
        <div className="col-12 col-lg-8">{this.renderFormBoxes()}</div>
      </div>
    );
  }
}

export default OnBoarding;
