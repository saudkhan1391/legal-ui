import React, { Component } from 'react';

import Introduction from 'components/pages/landing/Introduction';
import HowItWorks from 'components/pages/landing/HowItWorks';
import FindCase from 'components/pages/landing/FindCase';
import RecognizedLawyers from 'components/pages/landing/RecognizedLawyers';
import SafeData from 'components/pages/landing/SafeData';

class Landing extends Component {
  render() {
    return (
      <div className="landing-page">
        <Introduction />
        <HowItWorks />
        <FindCase />
        <RecognizedLawyers />
        <SafeData />
      </div>
    );
  }
}

export default Landing;
