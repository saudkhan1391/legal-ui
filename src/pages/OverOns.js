import React, { Component } from 'react';
import Hero from 'components/layout/Hero';

class OverOns extends Component {
  render() {
    return (
      <div className="over-ons-page">
        <Hero>
          <div className="container over-ons-hero-section">
            <h4 className="dark">Over ons</h4>
            <p className="dark">
              Lorem ipsum, or lipsum as it is sometimes known, is dummy text
              used
            </p>
          </div>
        </Hero>
        <div className="container over-ons-body">
          <p>
            Legal.nl is een initiatief van Axel Macro en HJ van der Tak: beide
            advocaat sinds begin jaren ’90, beide advocaat met een brede
            ervaring.
            <br />
            <br />
            Axel begon zijn carrière in 1990 bij het kantoor Pels Rijcken &
            Droogleever Fortuijn. Na inmiddels 30 jaar werkzaam te zijn in de
            advocatuur kan hij het best omschreven worden als een zeer ervaren
            allrounder. De laatste jaren heeft hij een gecombineerde proces- en
            adviespraktijk, en hij wordt met grote regelmaat door (buitenlandse)
            partijen ingeschakeld om als juridisch klankbord mee te werken aan
            de totstandkoming van – vaak complexe – transacties.
            <br />
            <br />
            HJ is sinds 1993 advocaat. Na zijn studies was hij eerder gedurende
            langere tijd werkzaam op commerciële en bedrijfsjuridische posities
            in de (inter)nationale film- en muziekindustrie. Na aan het
            Vondelpark een gecombineerde proces- en adviespraktijk te hebben
            gevoerd in intellectueel eigendomsrecht, mediarecht en
            entertainmentrecht, legt HJ van der Tak zich tegenwoordig als
            advocaat toe op strategiebepaling en begeleiding in
            IE-aangelegenheden. Naast advocaat zijn beide initiatiefnemers
            ervaren (internet)ondernemers met oog voor de belangen van de
            samenleving.
            <br />
            <br />
            Zo was HJ als initiator bij enkele spraakmakende en baanbrekende
            (internet)initiatieven betrokken, die zich ondermeer richten op
            innovatie van rechtspraak, rechtshulp en notariaat en Axel is
            betrokken geweest bij een groot aantal (internet) startups die
            gemeenschappelijk hadden dat ze gebaseerd waren op vernieuwende
            concepten.
            <br />
            <br />
            Het idee voor dit landelijke digitale platform komt voort uit hun
            eigen behoefte om de matchmaking tussen advocaten en rechtzoekenden
            beter, sneller en makkelijker te maken. Gezien hun uitgebreide
            ervaring in de advocatuur, denken beide initiatiefnemers dat zij
            daartoe een belangrijke bijdrage kunnen leveren. Mocht u ideeën
            hebben die verder aan dit doel bijdragen, dan horen zij bijzonder
            graag van u.
          </p>
          <h6>~ AXEL MACRO & HJ VAN DER TAK</h6>
        </div>
      </div>
    );
  }
}

export default OverOns;
