import React from 'react';
import { Link } from 'react-router-dom';

import AppBar from '@empire/hydra/components/AppBar';
import Overlay from '@empire/hydra/components/Overlay';
import Spacer from '@empire/hydra/components/Spacer';
import Button from '@empire/hydra/components/Button';

import LinkButton from 'components/core/LinkButton';
import Footer from 'components/layout/Footer';

const images = {
  logo: require('assets/images/logo-light.svg')
};

const styles = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center'
};

const renderAppbar = () => (
  <AppBar transparent onScrollSolid="50">
    <AppBar.Logo>
      <img src={images.logo} alt="logo" />
    </AppBar.Logo>
    <AppBar.ItemsHorizontal>
      <Spacer />
      <LinkButton className="mr-2">Home</LinkButton>
      <LinkButton className="mr-2">Over ons</LinkButton>
      <LinkButton className="mr-2">FAQs</LinkButton>
      <LinkButton className="mr-2">Contact</LinkButton>

      <Link to="/login">
        <Button bordered className="mr-2">
          Inloggen cliënt
        </Button>
      </Link>

      <Link to="/">
        <Button>Inloggen advocaat</Button>
      </Link>
    </AppBar.ItemsHorizontal>
    <AppBar.ItemsVertical>
      <div style={styles}>
        <LinkButton>Home</LinkButton>
        <LinkButton className="mt-2">Over ons</LinkButton>
        <LinkButton className="mt-2">FAQs</LinkButton>
        <LinkButton className="mt-2">Contact</LinkButton>
        <Link className="mt-2" to="/login">
          <Button bordered>Inloggen cliënt</Button>
        </Link>

        <Link className="mt-2" to="/">
          <Button>Inloggen advocaat</Button>
        </Link>
      </div>
    </AppBar.ItemsVertical>
  </AppBar>
);

const withMasterLayout = (WrappedComponent) => (props) => (
  <>
    <Overlay />
    {renderAppbar()}
    <div className="container-fluid no-padding">
      <WrappedComponent {...props} />
    </div>
    <Footer />
  </>
);

export default withMasterLayout;
