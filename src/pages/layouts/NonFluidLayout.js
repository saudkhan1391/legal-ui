import React from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import Cookies from 'js-cookie';

import AppBar from '@empire/hydra/components/AppBar';
import Overlay from '@empire/hydra/components/Overlay';
import Spacer from '@empire/hydra/components/Spacer';
import Button from '@empire/hydra/components/Button';

import { logoutUser } from '@empire/shared-ui/actions/appActions';
import { fetchGET } from '@empire/shared-ui/fetch';

import { appSettings } from 'settings';
import { compose } from 'redux';

const images = {
  logo: require('assets/images/logo.svg')
};

const styles = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center'
};

function logout(props) {
  fetchGET(`${appSettings.apiBaseUrl}/account/logout`, {
    onSuccess: () => {
      // Rremover UX improvement cookie
      Cookies.remove(appSettings.cookieName);

      toast.success('Logout');
      props.logoutUser();
      props.history.push('/login');
    }
  });
}

const renderAppBar = (props) => (
  <AppBar transparent onScrollSolid="50">
    <AppBar.Logo>
      <img src={images.logo} alt="logo" />
    </AppBar.Logo>
    <AppBar.ItemsHorizontal>
      <Spacer />

      <div className="dropdown">
        <Button>mijn account</Button>

        {props.user !== null && (
          <div className="dropdown-content">
            <Link onClick={() => logout(props)} className="dropdown-item">
              Logout
            </Link>
          </div>
        )}
      </div>
    </AppBar.ItemsHorizontal>
    <AppBar.ItemsVertical>
      <div style={styles}>
        <div className="dropdown">
          <Button>mijn account</Button>

          {props.user !== null && (
          <div className="dropdown-content">
            <Link onClick={() => logout(props)} className="dropdown-item">
              Logout
            </Link>
          </div>
          )}
        </div>
      </div>
    </AppBar.ItemsVertical>
  </AppBar>
);

const withNonFluidLayout = (WrappedComponent) => (props) => (
  <>
    <Overlay />
    {renderAppBar(props)}
    <div className="container">
      <WrappedComponent {...props} />
    </div>
  </>
);

withNonFluidLayout.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.app.user
});

const mapDispatchToProps = {
  logoutUser
};

const composedWrapper = compose(connect(mapStateToProps, mapDispatchToProps), withNonFluidLayout);

export default composedWrapper;
