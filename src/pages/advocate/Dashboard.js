import React, { Component } from 'react';

import ReactQuill from 'react-quill';

import Modal from '@empire/hydra/components/Modal';
import Button from '@empire/hydra/components/Button';
import Checkbox from '@empire/hydra/components/Checkbox';

import IconButton from 'components/core/IconButton';
import ViewSummons from 'components/Modals/ViewSummons';

const images = {
  sendImage: require('assets/images/send-image.svg')
};
class Dashboard extends Component {
  state = {
    petitionsList: [
      {
        title: 'Kees Koos'
      },
      {
        title: 'Kees Koos'
      },
      {
        title: 'Kees Koos'
      },
      {
        title: 'Kees Koos'
      }
    ],
    pitchesList: [
      {
        title: 'Sophie Bakker'
      },
      {
        title: 'Sophie Bakker'
      },
      {
        title: 'Sophie Bakker'
      }
    ],
    matchesList: [
      {
        title: 'Ahmet Fati',
        review: 'up'
      },
      {
        title: 'Ahmet Fati',
        review: 'down'
      },
      {
        title: 'Ahmet Fati',
        review: 'down'
      },
      {
        title: 'Ahmet Fati',
        review: 'up'
      }
    ],
    modalValue: false,
    modalTitle: '',
    currentModal: 0
  };

  renderNewPetitionsList() {
    const { petitionsList, modalValue } = this.state;

    return (
      <div className="col-12 col-md-4 inner-box">
        <label className="section-title">
          Nieuwe dagvaardingen/verzoekschriften
        </label>

        {petitionsList.map((item, i) => (
          <div key={i} className="list-box">
            <div className="box-title">
              <h6>{item.title}</h6>
              <label>
                <i className="far fa-clock" /> 1 uur geleden
              </label>
            </div>

            <ul>
              <li>
                <i className="fas fa-check" /> Ik ben de gedaagde
              </li>
              <li>
                <i className="fas fa-check" /> Ik heb een
                rechtsbijstandverzekering
              </li>
              <li>
                <i className="fas fa-times" /> Nee, geen recht op gesubsidieerde
                rechtsbijstand
              </li>
              <li>
                <i className="fas fa-times" /> Nee, geen voorkeurstaal
              </li>
            </ul>

            <div className="list-action">
              <Button
                className="secondary-button"
                onClick={() => this.setState({
                  modalValue: !modalValue,
                  modalTitle: 'Bekijk dagvaarding van Kees Koos',
                  currentModal: 0
                })}
              >
                Bekijk dagvaarding
              </Button>
              <Button
                onClick={() => this.setState({
                  modalValue: !modalValue,
                  modalTitle: 'Pitch naar cliënt',
                  currentModal: 1
                })}
              >
                Stuur pitch
              </Button>
            </div>
          </div>
        ))}
      </div>
    );
  }

  renderSentPitchesList() {
    const { pitchesList, modalValue } = this.state;

    return (
      <div className="col-12 col-md-4 inner-box">
        <label className="section-title">Verstuurde pitches</label>

        {pitchesList.map((item, i) => (
          <div key={i} className="list-box">
            <div className="box-title mb-2">
              <h6>{item.title}</h6>
            </div>

            <label>
              <i className="far fa-comments" />{' '}
              <a
                role="button"
                tabIndex="0"
                onClick={() => this.setState({
                  modalValue: !modalValue,
                  modalTitle: 'Pitch is verstuurd',
                  currentModal: 3
                })}
              >
                Pitch
              </a>{' '}
              is ontvangen
            </label>
          </div>
        ))}
      </div>
    );
  }

  renderMatchesList() {
    const { matchesList, modalValue } = this.state;

    return (
      <div className="col-12 col-md-4 inner-box">
        <label className="section-title">Matches</label>

        {matchesList.map((item, i) => (
          <div key={i} className="list-box">
            <div className="box-title mb-2">
              <h6>{item.title}</h6>
            </div>

            <label>
              <i className={`far fa-thumbs-${item.review}`} /> Gewonnen{' '}
              <a
                role="button"
                tabIndex="0"
                onClick={() => this.setState({
                  modalValue: !modalValue,
                  modalTitle: 'Pitch is verstuurd',
                  currentModal: 4
                })}
              >
                bekijk contactgegevens
              </a>
            </label>
          </div>
        ))}
      </div>
    );
  }

  renderViewSummonsModal() {
    const { currentModal } = this.state;
    return (
      <div className={`col-12 ${currentModal === 0 ? '' : 'd-none'}`}>
        <ViewSummons />
      </div>
    );
  }

  renderPitchToClientModal() {
    const { currentModal } = this.state;
    return (
      <div className={`col-12 ${currentModal === 1 ? '' : 'd-none'}`}>
        <ReactQuill
          placeholder="Start writing your pitch here....."
          // value={this.state.text}
          // onChange={this.handleChange}
        />

        <hr />

        <IconButton
          label="Verstuur pitch"
          onClick={() => this.setState({
            modalTitle: 'Pitch is verstuurd',
            currentModal: 2
          })}
        />
      </div>
    );
  }

  renderSendPitchModal() {
    const { currentModal } = this.state;

    return (
      <div className={`col-12 ${currentModal === 2 ? '' : 'd-none'}`}>
        <hr className="mt-0" />

        <label>
          Wij hebben uw pitch verstuurd naar de cliënt! Even afwachten of u
          wordt gekozen. Houd uw <span>e-mail</span> en
          <span>
            <a> dashboard </a>
          </span>
          in de gaten!
        </label>

        <img src={images.sendImage} width="100%" alt="logo" />

        <Checkbox label="Mag Frans u bellen voor feedback?" />
        <hr />

        <IconButton
          label="Terug naar overzicht"
          onClick={() => console.log('hello')}
        />
      </div>
    );
  }

  renderSendPitchReadOnlyModal() {
    const { currentModal } = this.state;

    return (
      <div
        className={`col-12 quill-ready-only ${
          currentModal === 3 ? '' : 'd-none'
        }`}
      >
        <ReactQuill
          readOnly
          value="Just readOnly"
          // onChange={this.handleChange}
        />

        <hr />

        <IconButton
          label="Terug naar overzicht"
          onClick={() => console.log('hello')}
        />
      </div>
    );
  }

  renderContactDetailsModal() {
    const { currentModal } = this.state;

    return (
      <div className={`col-12 ${currentModal === 4 ? '' : 'd-none'}`}>
        <hr className="mt-0" />

        <label>
          Ahmet Fati heeft voor u gekozen! De cliënt verwacht van u een
          belletje/e-mail voor vervolg.
        </label>

        <div className="detail">
          <h6 className="section-title">Gegevens van cliënt</h6>

          <label>Ahmet Fati</label>

          <label>06 1234567</label>

          <label>ahmet33@gmail.com</label>
        </div>

        <Checkbox label="Mag Frans u bellen voor feedback?" />
        <hr />

        <IconButton
          label="Terug naar overzicht"
          onClick={() => console.log('hello')}
        />
      </div>
    );
  }

  render() {
    const { modalValue, modalTitle } = this.state;
    return (
      <div className="page advocate-dashboard">
        <div className="row no-gutters">
          <div className="col-12">
            <div className="box">
              <div className="title">
                <h4>Overzicht cliënten</h4>
              </div>

              <div className="row no-gutters justify-content-between">
                {this.renderNewPetitionsList()}

                {this.renderSentPitchesList()}

                {this.renderMatchesList()}
              </div>
            </div>
          </div>

          <Modal
            show={modalValue}
            title={modalTitle}
            onClose={() => this.setState({ modalValue: false })}
          >
            <div className="row">
              {this.renderViewSummonsModal()}

              {this.renderPitchToClientModal()}

              {this.renderSendPitchModal()}

              {this.renderSendPitchReadOnlyModal()}

              {this.renderContactDetailsModal()}
            </div>
          </Modal>
        </div>
      </div>
    );
  }
}

export default Dashboard;
