import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';
import Cookies from 'js-cookie';
import { toast } from 'react-toastify';
import { connect } from 'react-redux';

import { updateUser } from '@empire/shared-ui/actions/appActions';
import { fetchPOST } from '@empire/shared-ui/fetch';

import Input from '@empire/hydra/components/Input';
import Checkbox from '@empire/hydra/components/Checkbox';
import Loader from '@empire/hydra/components/Loader';

import { appSettings } from 'settings';
import { generatePostAuthRoute } from 'lib/utils';

import IconButton from 'components/core/IconButton';
import AuthFooter from 'components/core/AuthFooter';

const images = {
  logo: require('assets/images/logo.svg')
};

class AdvocateLogin extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired
  };

  static propTypes = {
    updateUser: PropTypes.func.isRequired
  };

  componentDidMount() {}

  get history() {
    return this.props.history;
  }

  state = {
    email: '',
    password: '',
    hidePassword: true,
    rememberMe: false,
    loader: false
  };

  login = () => {
    const { email, password, rememberMe, loader } = this.state;

    if (email !== '' && password !== '') {
      this.setState({ loader: !loader }, () => {
        fetchPOST(
          `${appSettings.apiBaseUrl}/account/login`,
          {
            email,
            password,
            rememberMe
          },
          {
            stateContext: this,
            onSuccess: (json) => {
              this.setState({ loader: false }, () => {
                if (json.success) {
                  this.props.updateUser(json.data);

                  // Set UX improvement cookie
                  Cookies.set(appSettings.cookieName, '1', { expires: 365 });

                  toast.success('Login');

                  const route = generatePostAuthRoute(json.data);

                  this.history.push(route);
                } else {
                  toast.error('error');
                }
              });
            },
            onError: (error) => {
              this.setState({ loader: false }, () => {
                toast.error(`errors.${error}`);
              });
            }
          }
        );
      });
    } else {
      toast.error('Fields are empty');
    }
  };

  render() {
    const { loader, rememberMe } = this.state;

    return (
      <div className="page auth login">
        <div className="container">
          <div className="row justify-content-center no-gutters">
            <div className="col-11 col-sm-9 col-md-6 content">
              <img src={images.logo} alt="logo" />

              <div className="box">
                <div className="title">
                  <h5>Inloggen</h5>
                  <p>Welkom terug! Log in met uw e-mail adres.</p>
                </div>

                <form autoComplete="off">
                  <label>E-mailadres</label>
                  <Input
                    autoFocus
                    value={this.state.email}
                    ref={(r) => {
                      this.EmailField = r;
                    }}
                    onChange={(r) => this.setState({ email: r })}
                  />

                  <label>Wachtwoord</label>
                  <Input
                    type="password"
                    value={this.state.password}
                    ref={(r) => {
                      this.PasswordField = r;
                    }}
                    onChange={(r) => this.setState({ password: r })}
                  />

                  <Checkbox
                    label="Ingelogd blijven?"
                    value={rememberMe}
                    onChange={(r) => this.setState({ rememberMe: r })}
                  />

                  <hr />
                  {loader ? (
                    <Loader margin={false} />
                  ) : (
                    <IconButton
                      label="Inloggen bij legal.nl"
                      onClick={this.login}
                    />
                  )}
                </form>
              </div>

              <div className="box-footer">
                <a>Wachtwoord vergeten?</a>
                <Link to="/on-boarding">Nog geen account?</Link>
              </div>
            </div>
          </div>
        </div>
        <AuthFooter />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {
  updateUser
};

export default connect(mapStateToProps, mapDispatchToProps)(AdvocateLogin);
