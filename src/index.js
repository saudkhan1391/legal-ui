import React from 'react';
import ReactDOM from 'react-dom';

import './scss/main.scss';

import { Provider as ReduxProvider } from 'react-redux';

import App from './App';
import configureStore from './configureStore';

import 'react-toastify/dist/ReactToastify.css';

const store = configureStore();

ReactDOM.render(
  <ReduxProvider store={store}>
    <App />
  </ReduxProvider>,

  document.getElementById('root')
);
