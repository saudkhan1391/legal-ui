const express = require('express');
const path = require('path');
const compression = require('compression');
const shell = require('shelljs');
const helmet = require('helmet');
const prerender = require('prerender-node');
const redis = require('redis');

shell.exec('./env.sh');

const app = express();
const client = redis.createClient(process.env.REDIS_URL);

app.use(helmet());
app.use(compression());

prerender.crawlerUserAgents.push('googlebot');
prerender.crawlerUserAgents.push('bingbot');
prerender.crawlerUserAgents.push('yandex');
prerender.set('forwardHeaders', true);

const cacheableStatusCodes = {200: true, 302: true, 404: true};

prerender.set('beforeRender', (req, done) => {
  client.hmget(req.url, 'body', 'status', (err, fields) => {
    if (err || fields.filter(Boolean).length === 0) {
      console.log(`No cache hit for ${req.url}`);
      done(err);
    }
    else {
      console.log(`Cache hit for ${req.url}`);
      done(err, {body: fields[0], status: fields[1]});
    }
  });
}).set('afterRender', (err, req, prerenderRes) => {
  console.log(`Rendering status code ${prerenderRes.statusCode} for ${req.url}`);

  // Don't cache responses that might be temporary like 500 or 504.
  if (cacheableStatusCodes[prerenderRes.statusCode]) {
    console.log(`Caching ${req.url} with expiry 3600 sec`);
    client.hmset(req.url, 'body', prerenderRes.body, 'status', prerenderRes.statusCode);
    client.expire(req.url, 3600);
  }
});

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/build/index.html'));
});

app.use(express.static(`${__dirname}/build`));
app.use(express.static(`${__dirname}/public`));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '/build/index.html'));
});

app.listen(5000, () => console.log('Legal web app listening on port 5000!'));
